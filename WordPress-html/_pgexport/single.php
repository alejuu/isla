<?php
/*
 Template Name: project
 */
?>
<?php
get_header( 'smallheader' ); ?>

                <!-- Conteiner 1 - Full Bg Image : Start -->                 
                <div class="container-fluid container-project-1 container-center-full"> 
                    <h1 class="white"><?php _e( 'PROJECT NICARAGUA: SEMANA SANTA', 'isla' ); ?></h1> 
                </div>                 
                <!-- Conteiner 1 - Full Bg Image : End -->                 
                <!-- Conteiner 2 - White - FPO : Start -->                 
                <div class="container container-project-2"> 
                    <div class="row ch2-bounceInUp invisible"> 
                        <div class="col-md-6 col-sm-6"> 
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-project-fpo.png" /> 
                        </div>                         
                        <div class="col-md-6 col-sm-6"> 
                            <h3><?php _e( 'Since 2009, ISLA has been heavily involved with working with the Nicaraguan government, its people, and the Nicaraguan Red Cross to work with the country in developing a more sustainable lifeguard operation.', 'isla' ); ?></h3> 
                            <p><?php _e( 'Starting in 2012, ISLA’s began to implement its second of a three phase operation by implementing an annual 3-day intensive training course for a diverse range of Nicaraguan trainees. This group included civil service members, Red Cross lifeguard volunteers, and members from non-governmental agencies from across the country. The graduates of this program have been able to take their newly acquired skills, expertise and supplies to areas all over the country. The outcome has been a visibly drastic decrease in drownings in Nicaragua.', 'isla' ); ?></p> 
                            <p><?php _e( 'ISLA will continue its three part operation to the country and people of Nicaragua until they have a sustainable operating lifeguard program for their people.', 'isla' ); ?></p> 
                        </div>                         
                        <div class="col-md-12 col-margin-up"> 
                            <h2 class="blue text-center"><?php _e( 'We are committed to staying in the region as long as needed.', 'isla' ); ?></h2> 
                        </div>                         
                    </div>                     
                </div>                 
                <!-- Conteiner 2 - White - FPO : End -->                 
                <!-- Conteiner 3 - Blue - Squres : Start -->                 
                <div class="container-fluid container-project-3 blue-bg no-padding"> 
                    <div class="row eq-height ch3-bounceInUp invisible"> 
                        <div class="col-md-4 blue-extra-bg padding-twenty text-center"> 
                            <p class="white no-margin"><?php _e( 'ISLA has assisted in responding to', 'isla' ); ?></p> 
                            <h3 class="white no-margin"><?php _e( 'over 1000 refugees', 'isla' ); ?></h3> 
                            <p class="white no-margin"><?php _e( 'that have arrived to the shores of Lesvos this month. This includes both water and shore responses as well as minor medical aids.', 'isla' ); ?></p> 
                        </div>                         
                        <div class="col-md-4 blue-bg padding-twenty text-center"> 
                            <p class="white no-margin"><?php _e( 'ISLA has assisted in responding to', 'isla' ); ?></p> 
                            <h3 class="white no-margin"><?php _e( 'Over $35,000 in donations', 'isla' ); ?></h3> 
                            <p class="white no-margin"><?php _e( 'from ISLA volunteers towards critical lifesaving equipment to Lifeguard Hellas through their own personal fundraising efforts.', 'isla' ); ?></p> 
                        </div>                         
                        <div class="col-md-4 no-padding"> 
                            <div class="nicaragua-square-one"> 
</div>                             
                        </div>                         
                    </div>                     
                    <div class="row eq-height ch3-bounceInUp invisible"> 
                        <div class="col-md-4 blue-bg padding-twenty text-center"> 
                            <h3 class="white no-margin"><?php _e( '19 volunteers from', 'isla' ); ?></h3> 
                            <h3 class="white no-margin"><?php _e( '7 countries:', 'isla' ); ?></h3> 
                            <p class="white no-margin"><?php _e( 'Australia, Canada, Denmark, Switzerland, Turkey, United Kingdom, United States', 'isla' ); ?></p> 
                        </div>                         
                        <div class="col-md-4 container-center no-padding"> 
                            <div class="nicaragua-square-two"> 
</div>                             
                        </div>                         
                        <div class="col-md-4 blue-darker-bg padding-twenty text-center"> 
                            <p class="white no-margin"><?php _e( 'ISLA has sent', 'isla' ); ?></p> 
                            <h3 class="white no-margin"><?php _e( '4 different teams', 'isla' ); ?></h3> 
                            <p class="white no-margin"><?php _e( 'since January 2016', 'isla' ); ?></p> 
                        </div>                         
                    </div>                     
                    <div class="row eq-height ch3-bounceInUp invisible"> 
                        <div class="col-md-4 no-padding"> 
                            <div class="nicaragua-square-three"> 
</div>                             
                        </div>                         
                        <div class="col-md-4 blue-darker-bg padding-twenty text-center"> 
                            <p class="white no-margin"><?php _e( 'We are in the process of', 'isla' ); ?></p> 
                            <h3 class="white no-margin"><?php _e( 'planning 3 more teams', 'isla' ); ?></h3> 
                            <p class="white no-margin"><?php _e( 'to volunteer in the region', 'isla' ); ?></p> 
                        </div>                         
                        <div class="col-md-4 blue-extra-bg padding-twenty text-center"> 
                            <p class="white no-margin"><?php _e( 'ISLA volunteers along with Lifeguard Hellas are one of the few organizations in the region authorized by the Greek Coastguard to make rescues and provide aid.', 'isla' ); ?></p> 
                            <h3 class="white no-margin"><?php _e( 'Making our mission in the region more important than ever.', 'isla' ); ?></h3> 
                        </div>                         
                    </div>                     
                </div>                 
                <!-- Conteiner 3 - Blue - Members : End -->                 
                <!-- Conteiner 4 - White - Project content : Start -->                 
                <div class="container-fluid container-project-4"> 
                    <div class="container"> 
                        <div class="row ch4-bounceInUp invisible"> 
                            <div class="col-md-12 col-margin-down text-center"> 
                                <h2><?php _e( 'Project Nicaragua 2016', 'isla' ); ?></h2> 
                                <h5 class="text-uppercase"><?php _e( 'March 21 to March 28, 2016', 'isla' ); ?></h5> 
                                <div class="center-divider"></div>                                 
                            </div>                             
                            <div class="col-md-6 col-sm-6"> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-nicaragua-content-top.jpg" /> 
                            </div>                             
                            <div class="col-md-6 col-sm-6"> 
                                <h5 class="text-uppercase gray-light"><?php _e( 'Location', 'isla' ); ?></h5> 
                                <p><b><?php _e( 'Nicaragua:', 'isla' ); ?></b> <?php _e( 'Managua, Granada, Playa Popoyo', 'isla' ); ?></p> 
                                <h5 class="text-uppercase gray-light"><?php _e( 'Status', 'isla' ); ?></h5> 
                                <p><b><?php _e( 'Accepting Applications', 'isla' ); ?></b></p> 
                                <p><?php _e( 'Applications due by February 7, 2016', 'isla' ); ?></p> 
                                <p><?php _e( 'Volunteers announced March 5, 2016', 'isla' ); ?></p> 
                            </div>                             
                            <div class="col-md-12 col-sm-12 col-margin-up"> 
                                <h2 class="blue"><?php _e( 'Get pumped for another exciting Semana Santa in Nicaragua!', 'isla' ); ?></h2> 
                                <p><?php _e( 'Semana Santa in Nicaragua is a national holiday celebration traditionally associated with massive beach crowds, dangerous surf and ocean conditions, and multiple drowning events. This year we will be working alongside the Nicaraguan Red Cross on a Volunteer Lifeguard Mission during the holiday weekend.  During the volunteer Lifeguard Mission, both Nicaraguan guards and ISLA guards will live, share, and learn lifesaving techniques together! Prior to lifeguarding with the Red Cross, the team will have the opportunity to explore Granada together, exploring the lake Nicaragua Islands and touring the local volcanoes. After Semana Santa, members of the team may be traveling to Playa Popoyo to surf their hearts out for a few days! This is a great opportunity to visit a new place, meet incredible new friends, and share special skills that will help protect lives!', 'isla' ); ?></p> 
                            </div>                             
                        </div>                         
                    </div>                     
                </div>                 
                <!-- Conteiner 4 - White - Project content : End -->                 
                <!-- Conteiner 5 - Blue - Why : Start -->                 
                <div class="container-fluid container-project-5 blue-bg"> 
                    <div class="row ch5-bounceInUp invisible"> 
                        <div class="col-md-12 col-margin-down text-center"> 
                            <h2 class="white"><?php _e( 'Want to apply for this project?', 'isla' ); ?></h2> 
                            <p class="white"><?php _e( 'Our open project application periods are announced individually throughout the year via our email newsletter. Volunteer selection is HIGHLY competitive, so it is best to appliy as soon as the project application opens. Sign up for the ISLA newsletter to ensure you don’t get left behind!', 'isla' ); ?></p> 
                        </div>                         
                        <div class="col-md-12 col-margin-up text-center"> 
                            <button type="button" class="btn btn-primary">
                                <?php _e( 'SIGN UP NOW', 'isla' ); ?>
                            </button>                             
                        </div>                         
                    </div>                     
                </div>                 
                <!-- Conteiner 5 - Blue - Why : End -->                 
                <!-- Conteiner 6 - Gray Dark - Volunteers : Start -->                 
                <div class="container-fluid container-project-6 gray-dark-bg"> 
                    <div class="container"> 
                        <div class="row ch6-bounceInUp invisible"> 
                            <div class="col-md-12 col-margin-down text-center"> 
                                <h2 class="white"><?php _e( 'Project Nicaragua’s Past Volunteers', 'isla' ); ?></h2> 
                            </div>                             
                            <div class="col-md-12 volunteers text-center"> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-1.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-2.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-3.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-4.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-5.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-6.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-7.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-8.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-9.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-10.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-11.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-12.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-13.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-14.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-15.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-16.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-17.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-18.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-19.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-20.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-21.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-22.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-23.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-24.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-25.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-26.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-27.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-28.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-29.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-30.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-31.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-32.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-33.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-34.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-35.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-36.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-37.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-38.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-39.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-40.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-41.png" /> 
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/isla-volunteers-42.png" /> 
                            </div>                             
                        </div>                         
                    </div>                     
                </div>                 
                <!-- Conteiner 6 - Gray Dark - Volunteers : End -->                 
                <!-- Conteiner 7 - Gray Lighter - News : Start -->                 
                <div class="container-fluid container-project-7 gray-lighter-bg"> 
                    <div class="container no-padding"> 
                        <div class="row ch5-bounceInUp invisible"> 
                            <div class="col-md-12 col-margin-down text-center"> 
                                <h2><?php _e( 'Project Nicaragua 2014', 'isla' ); ?></h2> 
                                <h5 class="text-uppercase"><?php _e( 'April 17 to APRIL 20, 2014', 'isla' ); ?></h5> 
                                <div class="center-divider"></div>                                 
                            </div>                             
                            <div class="col-md-12 col-margin-down"> 
                                <p><?php _e( 'ISLA and Lifeguards without Borders sent a team of 10 volunteer lifeguards to the beaches of Nicaragua during the Easter Holiday. The team provided rescue equipment and assisted the Nicaragua Read Cross (NRC) in guarding the beaches of Jiquililo and Pase el Cabello. Before the holiday, the team explored the beautiful city of Granada, toured the lake Nicaragua Islands and checked out the local volcanoes. After the holiday, the team traveled to Playa Popoyo to surf their hearts out. This trip was a great opportunity to visit a new place, meet incredible new friends, and share special skills that will help protect lives!', 'isla' ); ?> </p> 
                            </div>                             
                            <div class="col-md-6 col-sm-6"> 
                                <div class="col-md-5 col-sm-12"> 
                                    <div class="img-news"></div>                                     
                                </div>                                 
                                <div class="col-md-7 col-sm-12"> 
                                    <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                    <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                    <div class="divider"></div>                                     
                                    <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                                </div>                                 
                            </div>                             
                            <div class="col-md-6 col-sm-6"> 
                                <div class="col-md-5 col-sm-12"> 
                                    <div class="img-news"></div>                                     
                                </div>                                 
                                <div class="col-md-7 col-sm-12"> 
                                    <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                    <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                    <div class="divider"></div>                                     
                                    <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                                </div>                                 
                            </div>                             
                            <div class="col-md-6 col-sm-6"> 
                                <div class="col-md-5 col-sm-12"> 
                                    <div class="img-news"></div>                                     
                                </div>                                 
                                <div class="col-md-7 col-sm-12"> 
                                    <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                    <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                    <div class="divider"></div>                                     
                                    <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                                </div>                                 
                            </div>                             
                            <div class="col-md-6 col-sm-6"> 
                                <div class="col-md-5 col-sm-12"> 
                                    <div class="img-news"></div>                                     
                                </div>                                 
                                <div class="col-md-7 col-sm-12"> 
                                    <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                    <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                    <div class="divider"></div>                                     
                                    <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                                </div>                                 
                            </div>                             
                        </div>                         
                    </div>                     
                </div>                 
                <!-- Conteiner 7 - Gray Lighter - News : End -->                 
                <!-- Conteiner 8 - White - News : Start -->                 
                <div class="container container-project-8"> 
                    <div class="row ch5-bounceInUp invisible"> 
                        <div class="col-md-12 col-margin-down text-center"> 
                            <h2><?php _e( 'Project Nicaragua 2013', 'isla' ); ?></h2> 
                            <h5 class="text-uppercase"><?php _e( 'March 24 to April 1, 2013', 'isla' ); ?></h5> 
                            <div class="center-divider"></div>                             
                        </div>                         
                        <div class="col-md-12 col-margin-down"> 
                            <p><?php _e( '6 ISLA volunteers patroled the beaches of Nicaragua alongside the Nicaraguan Red Cross / Red Crescent during the Easter Holiday. As part of an initiative to minimize deaths and injuries caused by drowning, ISLA taught lifesaving techniques, provided lifesaving equipment, and patrolled the beaches alongside the Nicaraguan Red Cross/Red Crescent.', 'isla' ); ?></p> 
                        </div>                         
                        <div class="col-md-6 col-sm-6"> 
                            <div class="col-md-5 col-sm-12"> 
                                <div class="img-news"></div>                                 
                            </div>                             
                            <div class="col-md-7 col-sm-12"> 
                                <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                <div class="divider"></div>                                 
                                <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                            </div>                             
                        </div>                         
                        <div class="col-md-6 col-sm-6"> 
                            <div class="col-md-5 col-sm-12"> 
                                <div class="img-news"></div>                                 
                            </div>                             
                            <div class="col-md-7 col-sm-12"> 
                                <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                <div class="divider"></div>                                 
                                <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                            </div>                             
                        </div>                         
                        <div class="col-md-6 col-sm-6"> 
                            <div class="col-md-5 col-sm-12"> 
                                <div class="img-news"></div>                                 
                            </div>                             
                            <div class="col-md-7 col-sm-12"> 
                                <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                <div class="divider"></div>                                 
                                <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                            </div>                             
                        </div>                         
                        <div class="col-md-6 col-sm-6"> 
                            <div class="col-md-5 col-sm-12"> 
                                <div class="img-news"></div>                                 
                            </div>                             
                            <div class="col-md-7 col-sm-12"> 
                                <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                <div class="divider"></div>                                 
                                <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                            </div>                             
                        </div>                         
                    </div>                     
                </div>                 
                <!-- Conteiner 8 - White - News : End -->                 
                <!-- Conteiner 9 - Gray Lighter - News : Start -->                 
                <div class="container-fluid container-project-9 gray-lighter-bg"> 
                    <div class="container no-padding"> 
                        <div class="row ch5-bounceInUp invisible"> 
                            <div class="col-md-12 col-margin-down text-center"> 
                                <h2><?php _e( 'Project Nicaragua 2012', 'isla' ); ?></h2> 
                                <h5 class="text-uppercase"><?php _e( 'March to April, 2012', 'isla' ); ?></h5> 
                                <div class="center-divider"></div>                                 
                            </div>                             
                            <div class="col-md-12 col-margin-down"> 
                                <p><?php _e( 'ISLA sent 17 volunteer lifeguards to conduct a Open Water Rescue Course in El Transito, Nicaragua. The participants in the first ever ISLA Open Water Lifeguard Course were the best volunteer lifeguards from Nicaraguan Red Cross/Red Crescent. The course included a First Aid and CPR practical exam, physical instruction, written test, and a non-buoy and buoy rescue practical exam. 25 lifeguard trainees completed the rigorous training course and received Basic ISLA Lifeguard Certification. After the training courses, ISLA volunteers traveled to the beaches of Poneloya, Jiquilillo, and El Transito to patrol alongside the newly trained volunteer lifeguards for the Easter Holiday, Semana Santa.', 'isla' ); ?></p> 
                            </div>                             
                            <div class="col-md-6 col-sm-6"> 
                                <div class="col-md-5 col-sm-12"> 
                                    <div class="img-news"></div>                                     
                                </div>                                 
                                <div class="col-md-7 col-sm-12"> 
                                    <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                    <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                    <div class="divider"></div>                                     
                                    <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                                </div>                                 
                            </div>                             
                            <div class="col-md-6 col-sm-6"> 
                                <div class="col-md-5 col-sm-12"> 
                                    <div class="img-news"></div>                                     
                                </div>                                 
                                <div class="col-md-7 col-sm-12"> 
                                    <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                    <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                    <div class="divider"></div>                                     
                                    <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                                </div>                                 
                            </div>                             
                            <div class="col-md-6 col-sm-6"> 
                                <div class="col-md-5 col-sm-12"> 
                                    <div class="img-news"></div>                                     
                                </div>                                 
                                <div class="col-md-7 col-sm-12"> 
                                    <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                    <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                    <div class="divider"></div>                                     
                                    <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                                </div>                                 
                            </div>                             
                            <div class="col-md-6 col-sm-6"> 
                                <div class="col-md-5 col-sm-12"> 
                                    <div class="img-news"></div>                                     
                                </div>                                 
                                <div class="col-md-7 col-sm-12"> 
                                    <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                    <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                    <div class="divider"></div>                                     
                                    <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                                </div>                                 
                            </div>                             
                        </div>                         
                    </div>                     
                </div>                 
                <!-- Conteiner 9 - Gray Lighter - News : End -->                 
                <!-- Conteiner 10 - White - News : Start -->                 
                <div class="container container-project-10"> 
                    <div class="row ch5-bounceInUp invisible"> 
                        <div class="col-md-12 col-margin-down text-center"> 
                            <h2><?php _e( 'Project Nicaragua 2011', 'isla' ); ?></h2> 
                            <h5 class="text-uppercase"><?php _e( 'March to April, 2011', 'isla' ); ?></h5> 
                            <div class="center-divider"></div>                             
                        </div>                         
                        <div class="col-md-12 col-margin-down"> 
                            <p><?php _e( 'ISLA sent 10 volunteers to lifeguard alongside the Nicaragua Red Cross during the Easter Holiday of 2011. Volunteers also enjoyed the sights and extreme adventures of of Managua, El Transito, Granada, Maltaglapa, and Popoyo.', 'isla' ); ?></p> 
                        </div>                         
                        <div class="col-md-6 col-sm-6"> 
                            <div class="col-md-5 col-sm-12"> 
                                <div class="img-news"></div>                                 
                            </div>                             
                            <div class="col-md-7 col-sm-12"> 
                                <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                <div class="divider"></div>                                 
                                <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                            </div>                             
                        </div>                         
                        <div class="col-md-6 col-sm-6"> 
                            <div class="col-md-5 col-sm-12"> 
                                <div class="img-news"></div>                                 
                            </div>                             
                            <div class="col-md-7 col-sm-12"> 
                                <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                <div class="divider"></div>                                 
                                <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                            </div>                             
                        </div>                         
                        <div class="col-md-6 col-sm-6"> 
                            <div class="col-md-5 col-sm-12"> 
                                <div class="img-news"></div>                                 
                            </div>                             
                            <div class="col-md-7 col-sm-12"> 
                                <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                <div class="divider"></div>                                 
                                <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                            </div>                             
                        </div>                         
                        <div class="col-md-6 col-sm-6"> 
                            <div class="col-md-5 col-sm-12"> 
                                <div class="img-news"></div>                                 
                            </div>                             
                            <div class="col-md-7 col-sm-12"> 
                                <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                <div class="divider"></div>                                 
                                <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                            </div>                             
                        </div>                         
                    </div>                     
                </div>                 
                <!-- Conteiner 10 - White - News : End -->                 
                <!-- Conteiner 11 - Gray Lighter - News : Start -->                 
                <div class="container-fluid container-project-11 gray-lighter-bg"> 
                    <div class="container no-padding"> 
                        <div class="row ch5-bounceInUp invisible"> 
                            <div class="col-md-12 col-margin-down text-center"> 
                                <h2><?php _e( 'Project Nicaragua 2010', 'isla' ); ?></h2> 
                                <h5 class="text-uppercase"><?php _e( 'March to April, 2010', 'isla' ); ?></h5> 
                                <div class="center-divider"></div>                                 
                            </div>                             
                            <div class="col-md-12 col-margin-down"> 
                                <p><?php _e( 'ISLA’s first trip to Nicaragua was a wild ride! We didn’t know what to expect, but 6 volunteers left to embark on the trip of a lifetime! The ISLA team spent the Easter Holiday  lifeguard alongside the Nicaragua Red Cross and having the time of their life.', 'isla' ); ?></p> 
                            </div>                             
                            <div class="col-md-6 col-sm-6"> 
                                <div class="col-md-5 col-sm-12"> 
                                    <div class="img-news"></div>                                     
                                </div>                                 
                                <div class="col-md-7 col-sm-12"> 
                                    <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                    <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                    <div class="divider"></div>                                     
                                    <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                                </div>                                 
                            </div>                             
                            <div class="col-md-6 col-sm-6"> 
                                <div class="col-md-5 col-sm-12"> 
                                    <div class="img-news"></div>                                     
                                </div>                                 
                                <div class="col-md-7 col-sm-12"> 
                                    <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                    <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                    <div class="divider"></div>                                     
                                    <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                                </div>                                 
                            </div>                             
                            <div class="col-md-6 col-sm-6"> 
                                <div class="col-md-5 col-sm-12"> 
                                    <div class="img-news"></div>                                     
                                </div>                                 
                                <div class="col-md-7 col-sm-12"> 
                                    <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                    <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                    <div class="divider"></div>                                     
                                    <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                                </div>                                 
                            </div>                             
                            <div class="col-md-6 col-sm-6"> 
                                <div class="col-md-5 col-sm-12"> 
                                    <div class="img-news"></div>                                     
                                </div>                                 
                                <div class="col-md-7 col-sm-12"> 
                                    <h6><?php _e( 'Heading 6', 'isla' ); ?></h6> 
                                    <h4><?php _e( 'Heading 4', 'isla' ); ?></h4> 
                                    <div class="divider"></div>                                     
                                    <p><?php _e( 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'isla' ); ?></p> 
                                </div>                                 
                            </div>                             
                        </div>                         
                    </div>                     
                </div>                 
                <!-- Conteiner 11 - Gray Lighter - News : End -->                 
                <!-- Conteiner 7 - Background Image : Start -->                 
                <div class="container-fluid container-home-7"> 
                    <div class="row ch7-bounceInUp invisible"> 
                        <div class="col-md-12 text-center"> 
                            <h2 class="white"><?php _e( 'Interested in traveling the globe as a humanitarian lifeguard?', 'isla' ); ?></h2> 
                            <h5 class="white"><?php _e( 'Congratulations, you’re at the first step to your ISLA adventure!  (Add some marketing copy explaining that to be a part of ISLA you HAVE TO sign up for the newsletter)', 'isla' ); ?></h5> 
                            <button type="button" class="btn btn-primary col-margin-up">
                                <?php _e( 'CTA TEXT HERE', 'isla' ); ?>
                            </button>                             
                        </div>                         
                    </div>                     
                </div>                 
                <!-- Conteiner 7 - Background Image : End -->                                 

<?php get_footer( 'smallheader' ); ?>