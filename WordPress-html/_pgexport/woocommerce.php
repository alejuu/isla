<?php
get_header( 'smallheader' ); ?>

<section class="shop-container"> 
    <div class="container-fluid"> 
        <div class="row"> 
            <div class="col-sm-3"> 
                <?php if ( is_active_sidebar( 'right_sidebar' ) ) : ?>
                    <aside id="main_sidebar">
                        <?php dynamic_sidebar( 'right_sidebar' ); ?>
                    </aside>
                <?php endif; ?> 
            </div>                             
            <div class="col-sm-9 col-md-9 col-store-content"> 
                <div>
                    <?php woocommerce_content() ?>
                </div>                                 
            </div>                             
        </div>                         
    </div>                     
</section>                                 

<?php get_footer( 'smallheader' ); ?>