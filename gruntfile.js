module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		// Copy web assets from bower_components to more convenient directories.
		copy: {
			main: {
				files: [
					// Vendor scripts.
					{
						expand: true,
						cwd: 'bower_components/bootstrap-sass/assets/javascripts/',
						src: ['**/*.js'],
						dest: 'scripts/bootstrap-sass/'
					}, {
						expand: true,
						cwd: 'bower_components/jquery/dist/',
						src: ['**/*.js', '**/*.map'],
						dest: 'scripts/jquery/'
					},
					// Fonts.
					{
						expand: true,
						filter: 'isFile',
						flatten: true,
						cwd: 'bower_components/',
						src: ['bootstrap-sass/assets/fonts/**'],
						dest: 'fonts/'
					},
					// Stylesheets
					{
						expand: true,
						cwd: 'bower_components/bootstrap-sass/assets/stylesheets/',
						src: ['**/*.scss'],
						dest: 'scss/'
					}
				]
			},
		},
		// Compile SASS files into minified CSS.
		sass: {
			options: {
				includePaths: ['bower_components/bootstrap-sass/assets/stylesheets']
			},
			dist: {
				options: {
					outputStyle: 'expanded'
				},
				files: {
					'css/app.css': 'scss/app.scss'
				}
			}
		},
		// Autoprefix Stylesheets.
		autoprefixer: {
      dist: {
        files: {
          'css/autoprefixer-style.css': 'css/app.css'
        }
      }
    },
		// Minify CSS.
		cssmin: {
		  options: {
		    shorthandCompacting: false,
		    roundingPrecision: -1
		  },
		  target: {
		    files: {
		      'style.css': ['css/autoprefixer-style.css']
		    }
		  }
		},
		// Concat minify JS.
		concat: {
			options: {
				separator: ';'
			},
			dist: {
				src: ['assets/js/scripts/*.js'],
				dest: 'assets/js/internal.js'
			}
		},
    uglify: {
      my_target: {
        files: {
          'assets/js/internal.min.js': ['assets/js/internal.js']
        }
      }
    },
		// Watch these files and notify of changes.
		watch: {
			grunt: {
				files: ['Gruntfile.js']
			},
			sass: {
				files: ['scss/**/*.scss'],
				tasks: ['sass']
			}
		}
	});
	// Load externally defined tasks.
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	// Establish tasks we can run from the terminal.
	grunt.registerTask('css', ['cssmin']);
	grunt.registerTask('js', ['concat','uglify']);
	grunt.registerTask('prefix', ['autoprefixer']);
	grunt.registerTask('style', ['sass','autoprefixer','cssmin']);
	grunt.registerTask('build', ['sass', 'copy']);
	grunt.registerTask('isla', ['style','js']);
	grunt.registerTask('default', ['build', 'watch']);
}
