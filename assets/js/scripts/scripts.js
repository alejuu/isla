// $() will work as an alias for jQuery() inside of this function
jQuery( document ).ready( function( $ ) {
  //On page ready
  $(document).ready(function() {
    //Animation on load
    $("html").addClass("fadeIn animated");
    //Mobile Nav
    $('[data-toggle=offcanvas]').click(function() {
      $('.row-offcanvas').toggleClass('active');
      $('.navbar-mob').toggle();
      if ($('.navbar-mob:visible').size() != 0) {
        $(".navbar-mob").addClass("fadeInLeft animated");
        $(".navbar-mob-toggle").addClass("fadeInLeft animated navbar-mob-toggle-after");
      }
      else {
        $(".navbar-mob-toggle").removeClass("navbar-mob-toggle-after").addClass("fadeInRight animated");
      }
    });
    //Height of divs = viewport
    function a() {
      windowHeight = $(window).innerHeight(), $(".container-home-1, .container-home-7, .container-about-1, .container-project-1, .container-members-1").css("min-height", windowHeight)
    }
    a(), $(window).resize(function() {
      a()
    })
  });
  //On Scroll
  $(window).scroll(function() {
    //Add classes to objects when apeearing in screen
    $('.ch2-bounceInUp').each(function() {
      var ch2 = $(this).offset().top;
      var topOfWindow = $(window).scrollTop();
      if (ch2 < topOfWindow+1000) {
        $(this).addClass("bounceInUp animated visible");
      }
    });
    $('.ch3-bounceInUp').each(function() {
      var ch3 = $(this).offset().top;
      var topOfWindow = $(window).scrollTop();
      if (ch3 < topOfWindow+1000) {
        $(this).addClass("bounceInUp animated visible");
      }
    });
    $('.ch4-bounceInUp').each(function() {
      var ch4 = $(this).offset().top;
      var topOfWindow = $(window).scrollTop();
      if (ch4 < topOfWindow+1000) {
        $(this).addClass("bounceInUp animated visible");
      }
    });
    $('.ch5-bounceInUp').each(function() {
      var ch5 = $(this).offset().top;
      var topOfWindow = $(window).scrollTop();
      if (ch5 < topOfWindow+1000) {
        $(this).addClass("bounceInUp animated visible");
      }
    });
    $('.ch6-bounceInUp').each(function() {
      var ch6 = $(this).offset().top;
      var topOfWindow = $(window).scrollTop();
      if (ch6 < topOfWindow+1000) {
        $(this).addClass("bounceInUp animated visible");
      }
    });
    $('.ch7-bounceInUp').each(function() {
      var ch7 = $(this).offset().top;
      var topOfWindow = $(window).scrollTop();
      if (ch7 < topOfWindow+1000) {
        $(this).addClass("bounceInUp animated visible");
      }
    });
    //Add classes to menus after scrolling
    var scroll = $(window).scrollTop();
    if (scroll >= 128) {
      $(".navbar-main").addClass("navbar-main-smaller");
      $(".navbar-secondary").addClass("navbar-secondary-top");
    } else {
      $(".navbar-main").removeClass("navbar-main-smaller");
      $(".navbar-secondary").removeClass("navbar-secondary-top");
    }
  });
} );
